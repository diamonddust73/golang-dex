package main

import (
	"fmt"
	"log"
	"github.com/PuerkitoBio/goquery"
	"regexp"
	"net/http"
	"io/ioutil"
)

func ScrapeMenuPages() {
	var validURL = regexp.MustCompile(`_tcm`)
	doc, err := goquery.NewDocument("https://nnu.sodexomyway.com/dining-choices/index.html")
	if err != nil {
		log.Fatal(err)
	}
	doc.Find("#main-content a").Each(func(i int, s *goquery.Selection) {
		link, ok := s.Attr("href")
		if ok {
			if validURL.MatchString(link) {
				PrintLink(link)
			}
		}
	})
}

func PrintLink(link string) {
	resp, err := http.Get("https://nnu.sodexomyway.com/" + link)
	if err != nil {
		// handle error
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", body)
}

func main() {
	ScrapeMenuPages()
}